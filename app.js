var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var keys = require('./config/keys');

mongoose.Promise = global.Promise;
var db = mongoose.connect(keys.mongoURI, {useMongoClient: true}, function(){
    console.log('db connected');
});

var app = express();
var port = process.env.PORT || 3000;

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// ROUTES
var books = require('./routes/books')();
app.use('/books', books);

app.get('/', (req, res) =>{
    res.send('welcome');
});

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
module.exports = app;
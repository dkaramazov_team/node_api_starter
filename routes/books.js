var express = require('express');
var router = require('express').Router();
var bodyParser = require('body-parser');
var Book = require('../models/book');
var BookCtrl = require('../controllers/bookCtrl')();

var routes = function () {
    router.route('/')
        .post(BookCtrl.post)
        .get(BookCtrl.get);
    router.use('/:id', (req, res, next) => {
        Book.findById(req.params.id, function (err, book) {
            if (err) {
                res.status(500).send(err);
            } else if (book) {
                req.book = book;
                next();
            } else {
                res.status(404).send('Not found');
            }
        });
    })
    router.route('/:id')
        .get((req, res) => {
            res.json(req.book);
        })
        .put((req, res) => {
            if (req.body._id) delete req.body._id;
            for (var p in req.body) {
                req.book[p] = req.body[p];
            }
            req.book.save((err, book) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(req.book);
                }
            });
        })
        .patch((req, res) => {
            if (req.body._id) delete req.body._id;
            for (var p in req.body) {
                req.book[p] = req.body[p];
            }
            req.book.save((err, book) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json(req.book);
                }
            });
        })
        .delete((req, res) => {
           req.book.remove((err) => {
                if(err){
                    res.status(500).send('Failed to remove');
                } else{
                    res.status(204).send('Removed');
                }
           }); 
        });
    return router;
}
module.exports = routes;
var should = require('should'),
    sinon = require('sinon');

describe('Book Controller Tests', function(){
    describe('Post', function(){
        it('should not allow empty title', function(){
            var Book = function(book){this.save()= function(){}};
            var req = {
                body: {
                    author: 'me'
                }
            };
            var res = {
                status: sinon.spy(),
                send: sinon.spy()
            }
            var BookCtrl = require('../controllers/bookCtrl')(Book);
            BookCtrl.post(req, res);
            res.status.calledWith(400).should.equal(true, `Bad Status ${res.status.args[0][0]}`)
            res.send.calledWith('Title is required').should.equal(true);
        })

    })
})